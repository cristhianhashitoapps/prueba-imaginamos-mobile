package com.hashitoapps.imaginamos_prueba;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

import api.ImaginamosApi;
import data.BaseHelper;
import entity.Usuario;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import util.Constantes;
import util.MatchAdapter;
import util.Message;
import util.UtilBitmap;

public class MenuActivity extends AppCompatActivity {

    private Button crear,lista,sincronizar;
    private BaseHelper baseHelper = null;
    private ProgressDialog pg= null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        crear  = (Button) findViewById(R.id.crear);
        lista  = (Button) findViewById(R.id.lista);
        sincronizar  = (Button) findViewById(R.id.sincronizar);
        pg = new ProgressDialog(this);

        crear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                crear();
            }
        });

        lista.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lista();
            }
        });

        sincronizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sincronizar();
            }
        });

    }

    private void sincronizar() {
        pg.setMessage("Espere por favor..");
        pg.show();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constantes.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ImaginamosApi api = retrofit.create(ImaginamosApi.class);

        List<Usuario>  usuarios = consultarUsuarios();

        Call<List<Usuario>> call = api.postUsers("auth", usuarios);

       call.enqueue(new Callback<List<Usuario>>() {
           @Override
           public void onResponse(Call<List<Usuario>> call, Response<List<Usuario>> response) {
               List<Usuario> usuarios = response.body();
               pg.setMessage("Usuarios enviados al servidor exitosamente.");
               pg.hide();
           }

           @Override
           public void onFailure(Call<List<Usuario>> call, Throwable t) {
                String i = "";
               pg.hide();
           }
       });


    }

    private List<Usuario> consultarUsuarios(){


        baseHelper = new BaseHelper(this, Constantes.getNombreBD(), null, Constantes.getVersionBd());
        SQLiteDatabase base; base = baseHelper.getWritableDatabase();
        List<Usuario> usuarios = null;

        if(!base.isOpen()) {
            base = baseHelper.getWritableDatabase();
        }
        try{

            Cursor cursor = base.rawQuery("select nombre,apellido,documento,cargo,salario,imagen,id from usuario",null);


            if(cursor.moveToFirst()){
                usuarios= new ArrayList<>();
                do{
                    Usuario usu = new Usuario();

                    usu.setNombre(cursor.getString(0));
                    usu.setApellido(cursor.getString(1));
                    usu.setDocumento(cursor.getString(2));
                    usu.setCargo(cursor.getString(3));
                    usu.setSalario(cursor.getString(4));

                    String ruta = Constantes.getRutaImagenes()+"/"+cursor.getString(5);
                    Bitmap bitmap = BitmapFactory.decodeFile(ruta);
                    UtilBitmap ubitmap = new UtilBitmap();
                    String bit = ubitmap.getStringFromBitmap(bitmap);

                    usu.setImagen(bit);

                    usu.setId(cursor.getString(6));
                    usuarios.add(usu);
                }while(cursor.moveToNext());
            }



        }
        catch(Exception ex){
            Log.d("exinsertbase",ex.getMessage());
        }
        finally {
            base.close();
        }

        return usuarios;

    }

    private void lista() {
        Intent i = new Intent();
        i.setClass(this,ListActivity.class);
        startActivity(i);

    }

    private void crear(){
        Intent i = new Intent();
        i.setClass(this,MainActivity.class);
        startActivity(i);
    }





}
