package com.hashitoapps.imaginamos_prueba;

import android.Manifest;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.master.permissionhelper.PermissionHelper;
import com.twilio.video.CameraCapturer;
import com.twilio.video.LocalVideoTrack;
import com.twilio.video.VideoView;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Random;

import data.BaseHelper;
import util.Constantes;
import util.Message;

public class MainActivity extends AppCompatActivity {

    private EditText nombre, apellido, documento, cargo, salario;

    //foto
    private CameraCapturer cameraCapturer;
    private LocalVideoTrack localVideoTrack;
    private VideoView videoView;
    private ImageButton takePictureButton;
    private PermissionHelper permissionHelper;
    private ImageView mImageView;
    private Button registrarse;

    private Bitmap globalBitmap = null;
    private BaseHelper baseHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        nombre = (EditText) findViewById(R.id.txtnombre);
        apellido = (EditText) findViewById(R.id.txtapellido);
        documento = (EditText) findViewById(R.id.txtdocumento);
        cargo = (EditText) findViewById(R.id.txtcargo);
        salario = (EditText) findViewById(R.id.txtsalario);
        takePictureButton = (ImageButton) findViewById(R.id.take_picture_button);
        mImageView = (ImageView)  findViewById(R.id.mImageView);
        videoView = (VideoView) findViewById(R.id.video_view);
        registrarse = (Button) findViewById(R.id.registrarse);

        registrarse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveUser();
            }
        });

        checkPermissions();
    }

    private final View.OnClickListener takePictureButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            takePicture();
        }
    };

    private void checkPermissions() {
        permissionHelper = new PermissionHelper(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 100);

        permissionHelper.request(new PermissionHelper.PermissionCallback() {
            @Override
            public void onPermissionGranted() {
                addCameraVideo();
            }

            @Override
            public void onPermissionDenied() {
                Log.d("tag", "onPermissionDenied() called");
                Message.show(getApplicationContext(), "Esta App necesita acceder a su cámara, por favor active los permisos.");
                finish();
            }

            @Override
            public void onPermissionDeniedBySystem() {
                Log.d("tag", "onPermissionDeniedBySystem() called");
                Message.show(getApplicationContext(), "Esta App necesita acceder a su cámara, por favor active los permisos.");
                finish();
            }
        });


    }

    private void addCameraVideo() {
        cameraCapturer = new CameraCapturer(this, CameraCapturer.CameraSource.BACK_CAMERA);
        localVideoTrack = LocalVideoTrack.create(this, true, cameraCapturer);
        localVideoTrack.addRenderer(videoView);
        takePictureButton.setOnClickListener(takePictureButtonClickListener);
    }

    private void takePicture() {
        cameraCapturer.takePicture(photographer);
    }

    private final CameraCapturer.PictureListener photographer =
            new CameraCapturer.PictureListener() {
                @Override
                public void onShutter() {

                }

                @Override
                public void onPictureTaken(byte[] bytes) {
                    globalBitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);

                    if (globalBitmap != null) {
                        showPicture(globalBitmap);
                        takePictureButton.setVisibility(View.INVISIBLE);
                        //btnShare.setVisibility(View.VISIBLE);
                        //btnNoShare.setVisibility(View.VISIBLE);
                    } else {
                        Toast.makeText(getApplicationContext(),
                                "fallo",
                                Toast.LENGTH_LONG).show();
                    }
                }

            };

    private void showPicture(final Bitmap bitmap) {
        videoView.setVisibility(View.INVISIBLE);
        mImageView.setImageBitmap(bitmap);
        mImageView.setVisibility(View.VISIBLE);
    }

    private void saveUser() {
        if(globalBitmap!=null){
            String imagen = saveImageOnDisk();
            if(!imagen.equals("")){

                baseHelper = new BaseHelper(this, Constantes.getNombreBD(), null, Constantes.getVersionBd());
                SQLiteDatabase base; base = baseHelper.getWritableDatabase();

                if(!base.isOpen()) {
                    base = baseHelper.getWritableDatabase();
                }
                try{
                    ContentValues valores = new ContentValues();

                    valores.put("nombre",nombre.getText().toString());
                    valores.put("apellido",apellido.getText().toString());
                    valores.put("documento",documento.getText().toString());
                    valores.put("cargo",cargo.getText().toString());
                    valores.put("salario", salario.getText().toString());
                    valores.put("imagen", imagen);

                    long idultimo= base.insert("usuario",null,valores);
                    Log.d("insertbase",idultimo+"");
                    Message.show(this,"Se grabó exitosamente el empleado");

                    finish();
                }
                catch(Exception ex){
                    Log.d("exinsertbase",ex.getMessage());
                }
                finally {
                    base.close();
                }


            }else{
                Message.show(this,"No se pudo grabar la foto en disco");
            }
        }else{
            Message.show(this,"Por favor tome una foto!");
        }
    }

    private String saveImageOnDisk(){

        String res = "";
        //String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(Constantes.getRutaImagenes());
        myDir.mkdirs();
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        String fname = "Image-"+ n +".jpg";
        File file = new File (myDir, fname);
        if (file.exists ()) file.delete ();
        try {
            FileOutputStream out = new FileOutputStream(file);
            globalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
            res=fname;
            Message.show(this,"Imagen grabada correctamente.");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    @Override
    //protected void onDestroy() {
    public void onDestroy() {
        if(videoView!=null && localVideoTrack!=null) {
            localVideoTrack.removeRenderer(videoView);
            if (localVideoTrack != null) {
                localVideoTrack.release();
                localVideoTrack = null;
            }
        }
        super.onDestroy();
    }
}
