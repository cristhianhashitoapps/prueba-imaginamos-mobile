package com.hashitoapps.imaginamos_prueba;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import data.BaseHelper;
import entity.Usuario;
import util.Constantes;
import util.MatchAdapter;

public class ListActivity extends AppCompatActivity {

    private ViewPager viewPager;
    private MatchAdapter adapter = null;
    private BaseHelper baseHelper = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        viewPager = (ViewPager) findViewById(R.id.view_pager);

        llenarAdapter();
    }

    private void llenarAdapter() {

        baseHelper = new BaseHelper(this, Constantes.getNombreBD(), null, Constantes.getVersionBd());
        SQLiteDatabase base; base = baseHelper.getWritableDatabase();

        if(!base.isOpen()) {
            base = baseHelper.getWritableDatabase();
        }
        try{

            Cursor cursor = base.rawQuery("select nombre,apellido,documento,cargo,salario,imagen,id from usuario",null);
            List<Usuario> usuarios = new ArrayList<Usuario>();

            if(cursor.moveToFirst()){
                do{
                    Usuario usu = new Usuario();

                    usu.setNombre(cursor.getString(0));
                    usu.setApellido(cursor.getString(1));
                    usu.setDocumento(cursor.getString(2));
                    usu.setCargo(cursor.getString(3));
                    usu.setSalario(cursor.getString(4));
                    usu.setImagen(cursor.getString(5));
                    usu.setId(cursor.getString(6));
                    usuarios.add(usu);
                }while(cursor.moveToNext());
            }

            if(usuarios.size()>0){
                adapter = new MatchAdapter(getSupportFragmentManager());
                adapter.setListMatch(usuarios);
                adapter.setContext(getApplicationContext());
                viewPager.setAdapter(adapter);
            }

        }
        catch(Exception ex){
            Log.d("exinsertbase",ex.getMessage());
        }
        finally {
            base.close();
        }


    }
}
