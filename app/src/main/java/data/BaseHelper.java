package data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Asus on 25/07/2017.
 */

public class BaseHelper extends SQLiteOpenHelper {

    private static String sentenciaCrearTablaUsuario = "create table usuario ("
            + "id integer primary key, nombre text, apellido text,"
            + "documento text,cargo text,salario text,imagen text);";

    private static String sentenciaEliminarTablaUsuario= "drop table if exists usuario;";


    public BaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            // crear
            db.execSQL(sentenciaCrearTablaUsuario);
            Log.e("basehelper", "se crea tabla usuario");
        } catch (Exception ex) {
            Log.e("basehelper-oncreate-ex", ex.getMessage());
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        try {
            db.execSQL(sentenciaEliminarTablaUsuario);
            db.execSQL(sentenciaCrearTablaUsuario);
        }
        catch (Exception ex) {
            Log.e("basehelper-oncreate-ex", ex.getMessage());
        }
    }
}

