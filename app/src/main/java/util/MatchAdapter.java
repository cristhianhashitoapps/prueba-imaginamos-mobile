package util;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

import entity.Usuario;

/**
 * Created by Asus on 25/07/2017.
 */

public class MatchAdapter extends FragmentStatePagerAdapter {

    FragmentManager fragmentManager;
    List<Usuario> listMatch;
    Context context;

    public MatchAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }


    @Override
    public int getCount() {
        return listMatch.size();
    }

    // Inflate the view for the fragment based on layout XML


    @Override
    public Fragment getItem(int position) {

        Usuario match = listMatch.get(position);
        MatchFragment fragment = MatchFragment.newInstance();
        fragment.setMatch(match);
        fragment.setContext(context);
        return fragment;
    }

    public List<Usuario> getListMatch() {
        return listMatch;
    }

    public void setListMatch(List<Usuario> listMatch) {
        this.listMatch = listMatch;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }
}
