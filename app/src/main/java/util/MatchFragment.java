package util;

import android.Manifest;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hashitoapps.imaginamos_prueba.R;
import com.master.permissionhelper.PermissionHelper;
import com.twilio.video.CameraCapturer;
import com.twilio.video.LocalVideoTrack;
import com.twilio.video.VideoView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import data.BaseHelper;
import entity.Usuario;

/**
 * Created by Asus on 25/07/2017.
 */

public class MatchFragment extends Fragment {

    private Usuario match;
    private EditText nombre, apellido, documento, cargo, salario, imagen;
    private ImageView imageView;
    private Context context;
    private PermissionHelper permissionHelper;
    private ImageButton takePictureButton;
    private Bitmap globalBitmap = null;
    private ImageView mImageView;
    private Button registrarse;
    private BaseHelper baseHelper;
    private String id;




    public static MatchFragment newInstance() {
        MatchFragment mf = new MatchFragment();
        return mf;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public Usuario getMatch() {
        return match;
    }

    public void setMatch(Usuario match) {
        this.match = match;
    }

    @Override
    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.user, container, false);
        nombre = (EditText) view.findViewById(R.id.txtnombre);
        apellido = (EditText) view.findViewById(R.id.txtapellido);
        documento = (EditText) view.findViewById(R.id.txtdocumento);
        cargo = (EditText) view.findViewById(R.id.txtcargo);
        salario = (EditText) view.findViewById(R.id.txtsalario);
        imageView = (ImageView)  view.findViewById(R.id.mImageView);
        takePictureButton = (ImageButton) view.findViewById(R.id.take_picture_button);
        mImageView = (ImageView)  view.findViewById(R.id.mImageView);
        registrarse = (Button) view.findViewById(R.id.registrarse);

        id = match.getId();
        nombre.setText(match.getNombre());
        apellido.setText(match.getApellido());
        documento.setText(match.getDocumento());
        cargo.setText(match.getCargo());
        salario.setText(match.getSalario());
        String ruta = Constantes.getRutaImagenes() +"/"+match.getImagen();

        File imgFile = new  File(ruta);

        if(imgFile.exists()){
            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            imageView.setImageBitmap(myBitmap);
        }

        registrarse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateUsuario();
            }
        });


        return view;
    }

    private void updateUsuario() {

        baseHelper = new BaseHelper(this.getActivity(), Constantes.getNombreBD(), null, Constantes.getVersionBd());
        SQLiteDatabase base; base = baseHelper.getWritableDatabase();

        if(!base.isOpen()) {
            base = baseHelper.getWritableDatabase();
        }
        try{
            ContentValues valores = new ContentValues();

            valores.put("nombre",nombre.getText().toString());
            valores.put("apellido",apellido.getText().toString());
            valores.put("documento",documento.getText().toString());
            valores.put("cargo",cargo.getText().toString());
            valores.put("salario", salario.getText().toString());

            String where = "id=?";
            String[] args = new String[]{id};

            long idultimo= base.update("usuario",valores,where,args);
            Log.d("updatebase",idultimo+"");
            Message.show(this.getActivity(),"Se actualizó exitosamente el empleado");

            //finish();
        }
        catch(Exception ex){
            Log.d("exinsertbase",ex.getMessage());
        }
        finally {
            base.close();
        }
    }


}