package api;

import java.util.List;

import entity.Usuario;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by Asus on 26/07/2017.
 */

public interface ImaginamosApi {

    @POST("crearUsuario")
    Call<Usuario> postUser(@Header("Authorization") String auth, @Body Usuario user);

    @POST("crearUsuarios")
    Call<List<Usuario>> postUsers(@Header("Authorization") String auth, @Body List<Usuario> lista);
}
